#!/bin/bash
scp -r ./lib/ root@146.185.162.95:/root/
scp -r ./public/ root@146.185.162.95:/root/contest-replay/
scp -r ./views/ root@146.185.162.95:/root/contest-replay/

scp ./index.js root@146.185.162.95:/root/contest-replay/
scp ./package.json root@146.185.162.95:/root/contest-replay/
scp ./rundocker.sh root@146.185.162.95:/root/contest-replay/
