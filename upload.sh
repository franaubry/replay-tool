#!/bin/bash

scp -r ./lib/ faubry@claus.info.ucl.ac.be:~/contest-replay/
scp -r ./public/ faubry@claus.info.ucl.ac.be:~/contest-replay/
scp -r ./views/ faubry@claus.info.ucl.ac.be:~/contest-replay/

scp ./index.js faubry@claus.info.ucl.ac.be:~/contest-replay/
scp ./package.json faubry@claus.info.ucl.ac.be:~/contest-replay/
scp ./rundocker.sh faubry@claus.info.ucl.ac.be:~/contest-replay/
