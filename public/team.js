function create_team(name, university, submissions) {
  var t = { };
  t.name = name;
  t.university = university;
  t.tries = [ ];
  t.times = [ ];
  for(var i = 0; i < submissions.length; i++) {
    var time_i = submissions[i][0];
    var tries_i = submissions[i][1];
    t.times.push(time_i);
    t.tries.push(tries_i);
  }
  return t;
}