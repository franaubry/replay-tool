function parseDJ(html) {
  var scoreboard = html.getElementsByClassName('scoreboard')[0]; 
  scoreboard = scoreboard.getElementsByTagName('tbody')[0];
  var rows = scoreboard.getElementsByTagName('tr');
  var nb_problems = rows[0].getElementsByClassName('score_cell').length;
  var teams = [ ];
  for(var i = 0; i < rows.length; i++) {
    var row = rows[i];
    var cols = row.getElementsByTagName('td');
    var link_to_name = cols[2].getElementsByTagName('a')[0];
    var span = link_to_name.getElementsByTagName('span');
    if(span.length == 2) {
      var prefix = span[0].textContent;
      var teamname = link_to_name.textContent;
      var university = span[1].textContent;
      teamname = teamname.substring(prefix.length, teamname.length - university.length);
    } else if(span.length == 1) {
      var prefix = '';
      var teamname = link_to_name.textContent;
      var university = span[0].textContent;
      teamname = teamname.substring(0, teamname.length - university.length);
    } else {
      var teamname = link_to_name.textContent;
      var university = '';
    }
    var cols = row.getElementsByClassName('score_cell');
    var solved = [ ];
    var reg = /^\d+$/;
    for(var j = 0; j < cols.length; j++) {
      var tmp = cols[j].textContent;
      if(tmp.length > 0) {
        var tries = cols[j].getElementsByTagName('span')[0].textContent;
        var time = tmp.substring(0, tmp.length - tries.length);
        if(reg.test(time)) {
          time = parseInt(time);
        } else {
          time = 1000000000;
        }
     		tries = parseInt(tries.split(' ')[0]);
      } else {
        var tries = 0;
        var time = 1000000000;
      }
      solved.push([time, tries]);
    }
    var team = create_team(teamname, university, solved);
    teams.push(team);
  }
  return {teams: teams, nb_problems: nb_problems};
}

function parseKATTIS(html) {
  var scoreboard = undefined;
  var tables = html.getElementsByTagName('table');
  for(var i = 0; i < tables.length; i++) {
    if(tables[i].id == 'standings') {
      scoreboard = tables[i];
    }
  }
  var thead = scoreboard.getElementsByTagName('thead')[0];
  // find team column
  var cols = thead.getElementsByTagName('th');
  var team_col = 1;
  for(var i = 0; i < cols.length; i++) {
    if(cols[i].textContent.trim() == 'Team') {
      team_col = i;
    }
  }
  var nb_problems = thead.getElementsByClassName('problemcolheader-standings').length;
  tbody = scoreboard.getElementsByTagName('tbody')[0];
  var rows = scoreboard.getElementsByTagName('tr');
  var teams = [ ];
  for(var i = 0; i < rows.length; i++) {
    var cols = rows[i].getElementsByTagName('td');
    if(rows[i].getElementsByClassName('rank').length == 0 || cols.length == 0) continue;
    var teamname = cols[team_col].textContent.trim();
    console.log(teamname);
    var solved = [ ];
    for(var j = 0; j < nb_problems; j++) {
      var col = cols[cols.length - j - 1];
      if(col.textContent.length == 0) {
        solved.push([1000000000, 0]);
      } else if(col.className == 'solved team_problem_cell' || col.className == 'solvedfirst team_problem_cell') {
        var tmp = col.getElementsByTagName('small')[0];
        var time = parseInt(tmp.textContent);
        tmp = col.innerHTML.split('\<br\>')[0];
        var tries = parseInt(tmp);
        solved.push([time, tries])
      } else {
        var tmp = col.innerHTML.split('\<br\>')[0];
        var tries = parseInt(tmp);
        solved.push([1000000000, tries]);
      }
    }
    solved = solved.reverse();
    var team = create_team(teamname, '', solved);
    teams.push(team);
  }
  return {teams: teams, nb_problems: nb_problems};
}

function parseYANDEX(root) {
  var tbody = root.getElementsByClassName('table__body')[0];
  //var nb_problems = tbody.getElementsByClassName('table__plain popupable i-bem').length;
  var rows = tbody.getElementsByTagName('tr');
  var nb_problems = rows[0].getElementsByTagName('td').length - 4;
  console.log('nb_problems: ' + nb_problems);
  var teams = [ ];
  for(var i = 0; i < rows.length; i++) {
    var cols = rows[i].getElementsByTagName('td');
    var teamname = cols[1].textContent;
    var solved = [ ];
    for(var j = 0; j < nb_problems; j++) {
      var col = cols[j + 2];
      var img = col.getElementsByTagName('img');
      if(img.length == 1) {
        img = img[0];
        if(img.className == 'image image_type_success') {
          var tmp = col.textContent;
          tmp = tmp.split(':');
          var h = parseInt(tmp[0]);
          var m = parseInt(tmp[1]);
          solved.push([60 * h + m, 1]);
        } else {
          solved.push([1000000000, 1]);
        }
      } else if(col.textContent == '—') {
        solved.push([1000000000, 0]);
      } else {
        var divs = col.getElementsByTagName('div');
        var tmp = divs[1].textContent;
        tmp = tmp.split(':');
        var h = parseInt(tmp[0]);
        var m = parseInt(tmp[1]);
        var tries_info = divs[0].textContent;
        if(tries_info[0] == '+') {
          if(tries_info.length == 1) {
            solved.push([60 * h + m, 1])
          } else {
            var tries = parseInt(tries_info.substring(1));
            solved.push([60 * h + m, tries + 1]);
          }
        } else {
          if(tries_info.length == 1) {
            solved.push([1000000000, 1])
          } else {
            var tries = parseInt(tries_info.substring(1));
            solved.push([1000000000, tries + 1]);
          }
        }
      }
    }
    var team = create_team(teamname, '', solved);
    teams.push(team);  
  }
  return {teams: teams, nb_problems: nb_problems};
}