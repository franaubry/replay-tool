var express = require('express');
var mongojs = require('mongojs');
var ObjectId = require('mongodb').ObjectID;
var credentials = require('./credentials');
var bodyParser = require('body-parser');
var db = mongojs(credentials.dburl, ['replays'] );
var formidable = require('formidable');

var fs = require('fs-extra');
const multer = require('multer');
const path = require('path');

// Set The Storage Engine
const storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function(req, file, cb) {
      cb(null, req.user.username + path.extname(file.originalname));
    }
});

// Init Upload
const upload = multer({
    storage: storage,
    limits:{fileSize: 1000000},
    fileFilter: function(req, file, cb){
      checkFileType(file, cb);
    }
}).single('profileImage');


// Set The Storage Engine
const storageResto = multer.diskStorage({
    destination: './public/restaurants/',
    filename: function(req, file, cb) {
      console.log('upload resto');
      cb(null, req.body.name + path.extname(file.originalname));
    }
});

const uploadResto = multer({
    storage: storageResto,
    limits:{fileSize: 1000000},
    fileFilter: function(req, file, cb) {
      console.log('file (in uploadResto): ' + file)
      checkFileType(file, cb);
    }
}).single('restoLogo');



// Check File Type
function checkFileType(file, cb){
    // Allowed ext
    const filetypes = /jpeg|jpg|png/;
    // Check ext
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    // Check mime
    const mimetype = filetypes.test(file.mimetype);
    if(mimetype && extname){
      return cb(null,true);
    } else {
      cb('Error: Images Only!');
    }
}


var expressValidator = require('express-validator');

// session packages
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var MongoDBStore = require('connect-mongodb-session')(session);

var exphbs  = require('express-handlebars');
var bcrypt = require('bcrypt');
const saltRounds = 10;
var app = express();

//app.use('/parsers', express.static(__dirname + '/node_modules/cropperjs/dist/'));

var hbs = exphbs.create({
    defaultLayout:'main',
    // Specify helpers which are only registered on this instance.
    helpers: {
        
        json: function(context) {
            return JSON.stringify(context);
        },

        alphabet: function(index) {
          return String.fromCharCode(65 + index);
        },

        times: function(n, block) {
          var accum = '';
          for(var i = 0; i < n; ++i)
              accum += block.fn(i);
          return accum;
        }
        
    }
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressValidator());
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');
app.use(express.static(__dirname + '/public'));


app.use('/lib', express.static(__dirname + '/lib/'));


var sessionStore = new MongoDBStore({
    uri: 'mongodb://admin:barbegrill4e@ds141221.mlab.com:41221/restaurants',
    collection: 'mySessions'
});
   
sessionStore.on('connected', function() {
    sessionStore.client; // The underlying MongoClient object from the MongoDB driver
});
   
// Catch errors
sessionStore.on('error', function(error) {
    assert.ifError(error);
    assert.ok(false);
});

app.use(session({
    secret: 'This is a secret',
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 7 // 1 week
    },
    store: sessionStore,
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(
  function(username, password, done) {
    db.admins.findOne({ username: username }, function(err, admin) {
      if(err) {
        console.log(err);
      }
      if(!admin) {
        return done(null, false);
      }
      const hash = admin.password;
      bcrypt.compare(password, hash, function(err, response) {
        if(response === true) {
          console.log('login user ' + admin.username);
          return done(null, admin);    
        } else {
          return done(null, false);
        }
      });
    });
  }
));
  
app.use(function(req, res, next) {
    res.locals.isAuthenticated = req.isAuthenticated();
    next();
});

app.set('port', process.env.PORT || 4000);

app.get('/', function(req, res) {
  res.render('index')
});

app.get('/simulator/:id', function(req, res) {
  db.replays.find( {identifier: req.params.id}, function(err, contests) {
    res.render('simulator', {contest: contests[0]});
  });
});

app.post('/simulator/:id', function(req, res) {
  console.log(req.params);
  var contest = req.body;
  contest.createTime = new Date();
  contest.identifier = req.params.id;
  db.replays.find( {identifier: contest.identifier}, function(err, contests) {
    if(err) console.log(err);
    if(contests.length == 0) {
      // the contest does not exist yet, create it
      console.log('new contest');
      db.replays.insert( contest, function(err, contest) {
        res.render('simulator');
      });
    } else {
      // the contest already exists, overwrite it
      console.log('already exists');
      const id = contests[0]._id;
      console.log(id);
      console.log(contest);
      db.replays.update({ _id: ObjectId(contests[0]._id) }, contest, function(err) {
        if(err) console.log(err);
        res.render('simulator');
      });  
    }
  });
});


// custom 404 page
app.use(function(req, res, next) {
    res.status(404);
    res.render('404');
});

// custom 500 page
app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500);
    res.render('500');
});

passport.serializeUser(function(user, done) {
    done(null, user);
});
  
passport.deserializeUser(function(user, done) {
    done(null, user);
});

function authenticationMiddleware () {  
	return (req, res, next) => {
		if (req.isAuthenticated()) return next();
	    res.redirect('/login')
	}
}


function manualAdminRegistration(username, password) {
  console.log('manual registration');
  bcrypt.hash(password, saltRounds, function(err, hash) {
    if(err) { console.log(err); }
    var admin = {
        'firstname': firstname,
        'lastname': lastname,
        'role': 'Admin',
        'username': username,
        'password': hash,
    };
    db.admins.insert(admin, function(err, res_db) {
        if(err) { console.log(err); }
        console.log('user registered')
    });
  });
}


app.listen(app.get('port'), function() {
    console.log('Express started on http://localhost:' + app.get('port') + '; press Ctrl-C to terminale.');
});
